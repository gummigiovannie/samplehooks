﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace Intercept {
    public partial class main : Form {
        private string value, content;
        IntPtr pointer;
        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
        public main() {
            InitializeComponent();
            new InterceptKeys();
            /* new InterceptMouse(); */
        /* - - - - - - */ }
        /* Intercept Clipboard */
        protected override void WndProc(ref System.Windows.Forms.Message m) {
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;
            switch (m.Msg) {
                case WM_DRAWCLIPBOARD:
                    SendMessage(pointer, m.Msg, m.WParam, m.LParam);
                    break;
                case WM_CHANGECBCHAIN:
                    if (m.WParam == pointer)
                        pointer = m.LParam;
                    else
                        SendMessage(pointer, m.Msg, m.WParam, m.LParam);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        /* - - - - - - */ }
    } }